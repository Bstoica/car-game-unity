﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	public string levelName2 = "Transfagarasan1";
	public string levelName = "MainMenu";
	
	public void Update()
	{
		if (Input.GetKeyDown ("escape")) {
			Application.LoadLevel (levelName);
		}
		
		if (Input.GetKeyDown ("r")) {
			Application.LoadLevel (levelName2);
		}
	}

}
